<section id="slider"><!--slider-->
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div id="slider-carousel" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
							<li data-target="#slider-carousel" data-slide-to="1"></li>
							<li data-target="#slider-carousel" data-slide-to="2"></li>
						</ol>
						
						<div class="carousel-inner">
							<div class="item active">
								<div class="col-sm-6">
									<h1>Palugada</h1>
									<h2>Koleksi Barang Terbaru</h2>
									<p>Koleksi Barang-Barang Terbaru Dari Ribuan Penjual</p>
									<button type="button" class="btn btn-default get">Dapatkan Sekarang</button>
								</div>
								<div class="col-sm-6">
									<img src="/images/home/girl1.jpg" class="girl img-responsive" alt="" />
									<img src="/images/home/pricing.png"  class="pricing" alt="" />
								</div>
							</div>
							<div class="item">
								<div class="col-sm-6">
									<h1>Palugada</h1>
									<h2>Gratis Ongkos Kirim</h2>
									<p>Gratis Ongkos Kirim Ke Seluruh Indonesia </p>
									<button type="button" class="btn btn-default get">Dapatkan Sekarang</button>
								</div>
								<div class="col-sm-6">
									<img src="/images/home/girl2.jpg" class="girl img-responsive" alt="" />
									<img src="/images/home/pricing.png"  class="pricing" alt="" />
								</div>
							</div>
							
							<div class="item">
								<div class="col-sm-6">
									<h1>Palugada</h1>
									<h2>Kumpulkan Poinmu</h2>
									<p>Kumpulkan Poin Belanjamu Dan Tukarkan Dengan Hadiah Menarik </p>
									<button type="button" class="btn btn-default get">Dapatkan Sekarang</button>
								</div>
								<div class="col-sm-6">
									<img src="/images/home/girl3.jpg" class="girl img-responsive" alt="" />
									<img src="/images/home/pricing.png" class="pricing" alt="" />
								</div>
							</div>
							
						</div>
						
						<a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
							<i class="fa fa-angle-left"></i>
						</a>
						<a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
							<i class="fa fa-angle-right"></i>
						</a>
					</div>
					
				</div>
			</div>
		</div>
	</section><!--/slider-->