	

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Home | Palugada</title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <link href="/css/prettyPhoto.css" rel="stylesheet">
    <link href="/css/price-range.css" rel="stylesheet">
    <link href="/css/animate.css" rel="stylesheet">
	<link href="/css/main.css" rel="stylesheet">
	<link href="/css/responsive.css" rel="stylesheet">     
    <link rel="shortcut icon" href="/images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="/images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>

@include('header')



<section id="form"><!--form-->
		<div class="container">
			<div class="row">
				<div class="col-sm-4 col-sm-offset-1">
					<div class="login-form"><!--login form-->
						<h2>Silahkan Login</h2>
						<form action="#" method="POST">
							<input type="text" name="user_username" placeholder="Name" value="<?php 
							if (isset($_SESSION['user_username'])) {
							echo $_SESSION['user_username'];
							} ?>"/>
							<input type="email" placeholder="Email Address" />
							<span>
								<input type="checkbox" class="checkbox"> 
								Ijinkan Tetap Masuk
							</span>
							<button type="submit" class="btn btn-default">Login</button>
						</form>
					</div><!--/login form-->
				</div>
				<div class="col-sm-1">
					<h2 class="or">Atau</h2>
				</div>
				<div class="col-sm-4">
					<div class="signup-form"><!--sign up form-->
						<h2>Daftar Pengguna Baru</h2>
						<form action="#">
							<input type="text" name="user_fullname" placeholder="Fullname"/>
							<input type="text" name="user_username" placeholder="Username"/>
							<input type="email" name="user_email" placeholder="Email Address"/>
							<input type="password" name="user_password" placeholder="Password"/>
							<button type="submit" name="user_submit" class="btn btn-default">Signup</button>
						</form>
					</div><!--/sign up form-->
				</div>
			</div>
		</div>
	</section><!--/form-->
	
	@include('footer')
  
    <script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/price-range.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>
</html>

<div id="first">
	<p> Sign In </p>
	<form action="register.php" method="POST">
	<input type="text" name="log_email" placeholder="Your Email ID" value="<?php 
	if (isset($_SESSION['log_email'])) {
		echo $_SESSION['log_email'];
	}
	?>" required> <br>
	<input type="password" name="log_password" placeholder="Your Password"> <br>
	<input type="submit" name="login_button" value="Login"> <br>


	<a href="#" id="signup" class="signup"> Need An Account? Register Here</a>

	<?php if (in_array("Email or password incorrect", $error_array)) echo "Email or password incorrect";  ?>
</form> <br>

</div>